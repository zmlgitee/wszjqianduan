import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import myNav from './components/Nav.vue'
import myHeader from './components/Header.vue'
import myFooter from './components/Footer.vue'
import myValid from './views/user/valid'
import axios from 'axios'
  
Vue.config.productionTip = false
Vue.component('my-nav',myNav)
Vue.component('my-header',myHeader)
Vue.component('my-footer',myFooter)
Vue.component('my-valid',myValid)
Vue.use(ElementUI)
// axios.defaults.baseURL="http://127.0.0.1:3000"
axios.defaults.baseURL="http://zmlly.cn/api/"
Vue.prototype.axios=axios
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
