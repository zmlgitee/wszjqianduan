import Vue from 'vue'
import VueRouter from 'vue-router'
import Index from '../views/Index.vue'
import Page1 from '../views/index/page1.vue'
import endIndex from '../views/back_end/end_index.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    component:Index,
    children:[
      {
        path:'',
        component:Page1
      },{
    path:'/commodity/:cid',
    component: () => import(
      /* webpackChunkName: "commodity" */ 
      '../views/Commodity.vue'
    ),
    props:true
  },
  {
    path:'/about',
    component: () => import(
      /* webpackChunkName: "about" */ 
      '../views/About.vue'
    ),
  },
  {
    path:'/details/:did',
    component: () => import(
      /* webpackChunkName:"details" */ 
      '../views/Details.vue'
    ),
    props:true
  },
  {
    path:'/service',
    component: () => import(
      /* webpackChunkName: "service" */ 
      '../views/Service.vue'
    ),
  },
  {
    path:'/news',
    component: () => import(
      /* webpackChunkName: "news" */ 
      '../views/News.vue'
    ),
  },
  {
    path:'/picture',
    component: () => import(
      /* webpackChunkName: "picture" */ 
      '../views/Picture.vue'
    ),
  },
    ]
  },
  {
    path:'/admin',
    component: () => import(
      /* webpackChunkName: "admin" */ 
      '../views/back_end/back_end.vue'
    ),
    children:[
      {
        path:'/',
        component:endIndex
      },{
        path:'/user',
        component: () => import(
          /* webpackChunkName: "user" */ 
          '../views/back_end/end_user.vue'
        ),
      },{
        path:'/adminorder/:uid',
        component: () => import(
          /* webpackChunkName: "adminorder" */ 
          '../views/back_end/end_order.vue'
        ),
        props:true
      },
      {
        path:'/whole_order',
        component: () => import(
          /* webpackChunkName: "whole_order" */ 
          '../views/back_end/end_whole.vue'
        ),
      },
      {
        path:'/admin_shop',
        component: () => import(
          /* webpackChunkName: "admin_shop" */ 
          '../views/back_end/end_shop.vue'
        ),
      },
      {
        path:'/admin_news',
        component: () => import(
          /* webpackChunkName: "admin_news" */ 
          '../views/back_end/end_news.vue'
        ),
      }
    ]
  }
]

const router = new VueRouter({
  routes
})

export default router
