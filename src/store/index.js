import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    uid:-1,
    phone:"",
    information:[],
    title:'首页'
  },
  mutations: {
    setUid(state,uid){
      state.uid=uid
    },
    setPhone(state,phone){
      state.phone=phone
    },
    setInformation(state,information){
      state.information=information
    },
    setTitle(state,title){
      state.title=title
    }
  },
  actions: {
  },
  modules: {
  }
})
