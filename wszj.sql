#设置服务器端链接客户端的编码为utf-8
set names utf8;
#丢弃数据库,如果存在
drop database if exists wszj;
#创建数据库,设置存储编码为utf-8
create database wszj charset=utf8;
#进入数据库
use wszj;
#1-创建首页轮播图表
create table ws_index_rotation(
  id int primary key auto_increment,
  rotation_pic varchar(64),  #轮播图片
  rotation_title varchar(16)  #标题
);



#2-创建购物车表
create table ws_shop(
  id int primary key auto_increment,
  shop_title varchar(32),
  shop_price decimal(6,2),
  shop_pic varchar(64),
  shop_num int,
  shop_color varchar(8),
  phone varchar(16)
);



#3-创建地址表
create table ws_address(
  id int primary key auto_increment,
  address_name varchar(8),
  address_phone varchar(16),
  address_address varchar(64),
  phone varchar(16)
);


#4-创建订单表
create table ws_order(
  id int primary key auto_increment,
  order_title varchar(32),
  order_price decimal(6,2),
  order_pic varchar(64),
  order_num int,
  address_name varchar(8),
  address_phone varchar(16),
  address_address varchar(64),
  order_state int,
  order_after int,
  order_color varchar(8),
  phone varchar(16)
);


#5-创建商品详情表
create table ws_commodity(
  id int primary key auto_increment,
  com_pic varchar(64),
  com_title varchar(32),
  com_introduction varchar(64),
  com_oldprice decimal(6,2),
  com_newprice decimal(6,2),
  com_sales int,
  com_bigpic varchar(64),
  com_package varchar(8),
  com_size_1 varchar(64),
  com_size_2 varchar(64),
  com_category varchar(4)
);

insert into ws_commodity values(null,'shubiao1.png','W70 MAX','灵敏调校RGB彩漫鼠',259.00,199.00,99,'details_z01.jpg','官方套餐','details_b01.jpg','details_b02.jpg','10');
insert into ws_commodity values(null,'jianpan1.png','B930','
光轴RGB彩漫电竞机械键盘',599.00,399.00,1099,'jianpan_e01.jpg','官方套餐','jianpan_d01.jpg','jianpan_d02.jpg','20');
insert into ws_commodity values(null,'erji1.png','J450','
炫光游戏耳麦',299.00,159.00,263,'erji_1d01.jpg','官方套餐','erji_1e01.jpg','','30');

#6-创建商品特征表
create table ws_features(
  id int primary key auto_increment,
  feat_details varchar(256),
  feat_pic varchar(64),
  feat_title varchar(32),
  com_id int
);

insert into ws_features values(null,'搭载BC3332-A 10K定制引擎，100至10000 CPI可调，帧速率8000 fps，加速度35g，循踪速度250 ips，专为游戏打造。','details_d01.jpg','BC3332-A 10K定制引擎',1);
insert into ws_features values(null,'因不同游戏对鼠标指针的高刷新率需求，通过调整不同报告率，使鼠标移动灵顺，移动细节更多，且定位更稳准。','details_d02.jpg','2000 Hz 报告率切换',1);
insert into ws_features values(null,'因不同鼠标垫表面折射光不同，通过调整传感器物距感应高度后，杜绝移动及停止移动中不产生指针抖动，虚飘，迟钝等定位问题。','details_d03.jpg','静默高度LOD切换',1);
insert into ws_features values(null,'可编辑5种RGB彩漫灯效，切换时将鼠标抬起，然后按［1］键循环切换。','details_d04.jpg','免驱5种RGB彩漫',1);
insert into ws_features values(null,'食指快速切换火力轮的左键三种点击(1/N/3）,改进了按键1/N/3切换时，食指恐因误触及距离等因素而影响火力的输出。','details_d05.jpg','临阵火力轮',1);
insert into ws_features values(null,'快速瞄准狙杀。软件扩展六种狙击模式，提升狙击技能。达到快而稳准的目标狙杀。','details_d06.jpg','临阵狙击键',1);

insert into ws_features values(null,'创新的LK(Light Strike)天平光轴技术,利用平衡杆可稳定控制按键的直下直上移动，不因倾斜摩擦形成的偏重卡键失误，触发更快更准，助你快人一步！专利号: CN201620671497.4','jianpan_c01.jpg','LK3天平光轴技术',2);
insert into ws_features values(null,'光轴键程3mm,下压1.8mm导通， 比传统金属机械轴触发快25%; (传统机械键程4mm, 下压2.2mm导通）','jianpan_c02.jpg','触发-快25%',2);
insert into ws_features values(null,'红外光学感应导通，触发信号没有杂讯， 按键动作没有延迟。(传统金属、机械杂讯令响应延迟18~30毫秒)。','jianpan_c03.jpg','光轴比机械轴快30ms',2);
insert into ws_features values(null,'当按柄下压至1.8mm，光轴瞬间导通同时发出"鸣脆枪声"，更为竞技带来激情动感。','jianpan_c04.jpg','特创"鸣脆枪声"触感',2);
insert into ws_features values(null,'光轴无金属触点，按键寿命亿次点击。(传统机械键盘，金属触点易氧化磨损，寿命限于数千万次)。','jianpan_c05.jpg','1亿次按键寿命',2);
insert into ws_features values(null,'RGB彩漫背光系统提供3种调灯模式，每颗按键配备独立背光1，680 万色可调，搭配“RGB彩漫”控键宝典轻松个性化设置。','jianpan_c06.jpg','自定义 RGB彩漫',2);
insert into ws_features values(null,'内置记忆多种动漫灯效模式，通过“Fn+F12”循环切换，让您的战斗激情更加精彩。','jianpan_c07.jpg','免驱多种 RGB灯效',2);
insert into ws_features values(null,'RGB灯库提供全彩、键彩、及彩漫不同键盘万彩背光展示，利用Fn组合热键切换10种喜欢的RGB灯效。上传/下载“RGB灯效”，通过下载玩家编辑好的灯效，打造个性化万彩缤纷的RGB彩漫键盘','jianpan_c08.jpg','RGB灯库 万彩缤纷',2);
insert into ws_features values(null,'创新“Full Anti-Ghost”全键无冲技术，在游戏中可任按各种技能输出按键，都能准确快速响应，制敌机先！','jianpan_c09.jpg','智能全无冲设计',2);
insert into ws_features values(null,'采用排水槽+排水孔双重排水防护，使意外渗透液体从不同方向聚集至多处排水孔有效排出。','jianpan_c10.jpg','双排水设计',2);
insert into ws_features values(null,'PCB线路板表面形成一道强而有力的超疏水纳米涂膜，有效阻止线路板上元器件涉水受潮以及被酸碱盐腐蚀的情况。','jianpan_c11.jpg','防水纳米涂层',2);
insert into ws_features values(null,'主打电竞风格，体积上更加小巧紧凑，节省桌面空间！','jianpan_c12.jpg','87键紧凑设计',2);

insert into ws_features values(null,'七种循环呼吸式灯效，金属的黑色铁网，透着艳丽的灯光，让你时刻炫酷到底。','erji_1c01.jpg','炫酷七彩循环呼吸灯',3);
insert into ws_features values(null,'调节音质变的更丰富而活力，有效扩展厚实低频延迟混响。','erji_1c02.jpg','等容双腔设计展现浑厚低音',3);
insert into ws_features values(null,'40毫米扬声器包含钕铁硼磁体和高张力CCAW线圈。独特的激光微调隔膜提供了一个非常准确的游戏音讯再现。','erji_1c03.jpg','精制40mm驱动单元',3);
insert into ws_features values(null,'USB接口，防缠绕线材，音质清晰','erji_1c04.jpg','USB接口',3);
insert into ws_features values(null,'悬浮软皮革头巾精心设计为所有类型的头部形状提供一个舒适和舒适的适合使用期间没有张力。','erji_1c05.jpg','悬浮式头挽',3);
insert into ws_features values(null,'高端防噪音和高灵敏度麦克风，让您跟队友交流音质清晰自然。','erji_1c06.jpg','防噪音麦克风',3);

#insert into ws_features values(null,'','details_d0.jpg','',1);

#7-创建商品特点表
create table ws_characteristic(
  id int primary key auto_increment,
  chara_details varchar(64),
  chara_pic varchar(64),
  com_id int
);

insert into ws_characteristic values(null,'BC3332-A 定制引擎','details_c01.png',1);
insert into ws_characteristic values(null,'三档静默高度','details_c02.png',1);
insert into ws_characteristic values(null,'2000 Hz 报告率可调','details_c03.png',1);
insert into ws_characteristic values(null,'双色软胶 滚轮轴','details_c04.png',1);
insert into ws_characteristic values(null,'响应1ms','details_c05.png',1);
insert into ws_characteristic values(null,'4M记忆体','details_c06.png',1);

insert into ws_characteristic values(null,'1亿次耐用点击','jianpan_b01.png',2);
insert into ws_characteristic values(null,'按键响应0.2ms','jianpan_b02.png',2);
insert into ws_characteristic values(null,'免驱多段灯效切换','jianpan_b03.png',2);
insert into ws_characteristic values(null,'LK 枪声技术','jianpan_b04.png',2);
insert into ws_characteristic values(null,'三代光轴技术','jianpan_b05.png',2);
insert into ws_characteristic values(null,'自定RGB彩漫','jianpan_b06.png',2);

insert into ws_characteristic values(null,'高质量7.1 音效','erji_1b01.png',3);
insert into ws_characteristic values(null,'柔软耳罩','erji_1b02.png',3);
insert into ws_characteristic values(null,'降噪麦克风','erji_1b03.png',3);
insert into ws_characteristic values(null,'可调节头挽','erji_1b04.png',3);
insert into ws_characteristic values(null,'防缠绕线材','erji_1b05.png',3);

#insert into ws_characteristic values(null,'','details_c0_.png',1);

#8-创建商品颜色表
create table ws_color(
  id int primary key auto_increment,
  color varchar(8),
  com_id int
);

insert into ws_color values(null,'黑色',1);
insert into ws_color values(null,'白色',1);

insert into ws_color values(null,'黑底黑帽',2);

insert into ws_color values(null,'红色',3);
insert into ws_color values(null,'绿色',3);

#insert into ws_color values(null,'',1);

#9-创建商品轮播表
create table ws_commodity_rotation(
  id int primary key auto_increment,
  pic varchar(64),
  com_id int
);

insert into ws_commodity_rotation values(null,'details_r01.jpg',1);
insert into ws_commodity_rotation values(null,'details_r02.jpg',1);
insert into ws_commodity_rotation values(null,'details_r03.jpg',1);
insert into ws_commodity_rotation values(null,'details_r04.jpg',1);

insert into ws_commodity_rotation values(null,'jianpan_a01.jpg',2);
insert into ws_commodity_rotation values(null,'jianpan_a02.jpg',2);
insert into ws_commodity_rotation values(null,'jianpan_a03.jpg',2);
insert into ws_commodity_rotation values(null,'jianpan_a04.jpg',2);

insert into ws_commodity_rotation values(null,'erji_1a01.jpg',3);
insert into ws_commodity_rotation values(null,'erji_1a02.jpg',3);
insert into ws_commodity_rotation values(null,'erji_1a03.jpg',3);

#insert into ws_commodity_rotation values(null,'details_r01.jpg',1);

#10-创建商品参数表
create table ws_parameter(
  id int primary key auto_increment,
  parameter varchar(32),
  com_id int
);

insert into ws_parameter values(null,'传感器：BC3332-A 10K定制引擎',1);
insert into ws_parameter values(null,'分辨率：100至10000 CPI可调',1);
insert into ws_parameter values(null,'帧速率：8000 fps',1);
insert into ws_parameter values(null,'加速度：35g',1);
insert into ws_parameter values(null,'循踪速度 : 250 ips',1);
insert into ws_parameter values(null,'报告率：125~2000Hz',1);
insert into ws_parameter values(null,'按键反应：1毫秒',1);
insert into ws_parameter values(null,'按键寿命 : 5000万次（左/右键）',1);
insert into ws_parameter values(null,'滚轮寿命：50万圈',1);
insert into ws_parameter values(null,'灵敏金靴 : 行走300公里',1);

insert into ws_parameter values(null,'键盘端口：USB',2);
insert into ws_parameter values(null,'键盘背光：1680万色RGB ',2);
insert into ws_parameter values(null,'彩漫灯效：自设10组彩漫灯效(需软件支持)',2);
insert into ws_parameter values(null,'免驱灯效：6种RGB灯效(带记忆)',2);
insert into ws_parameter values(null,'背光亮度：多段可调(带记忆)',2);
insert into ws_parameter values(null,'反应速度： 0.2ms',2);
insert into ws_parameter values(null,'按键类别: 光学导通',2);
insert into ws_parameter values(null,'键盘热键：Fn组合键',2);
insert into ws_parameter values(null,'防水性能: 溅水设计',2);
insert into ws_parameter values(null,'背光亮度：多段可调',2);
insert into ws_parameter values(null,'游戏键帽：8颗H型游戏键帽',2);
insert into ws_parameter values(null,'防冲键：全键防冲',2);
insert into ws_parameter values(null,'键盘线长：1.8 米',2);
insert into ws_parameter values(null,'按键寿命：1亿次',2);
insert into ws_parameter values(null,'按键加固：螺丝锁固空格键',2);
insert into ws_parameter values(null,'系统支持: Windows XP/Vista/7/8/8.1/10',2);

#insert into ws_parameter values(null,'定位性能',1);

#11-创建用户表
create table ws_user(
  id int primary key auto_increment,
  phone varchar(16),
  user_id varchar(32),
  user_pwd varchar(16),
  user_names varchar(8),
  user_uname varchar(16),
  user_sex boolean
);

insert into ws_user values(null,'15139119610','','z123456','','',1);
insert into ws_user values(null,'8888','','8888','','',1);

#12-创建新闻表
create table ws_news(
  id int primary key auto_increment,
  news_title varchar(32),
  news_date varchar(32),
  news_person varchar(16),
  news_content varchar(512),
  news_href varchar(128)
);

insert into ws_news values(null,'“三位一体”助力玩家游戏制胜，血手幽灵G575游戏耳机评测','2021-07-22','太平洋电脑网','暑假来临，相信不少小伙伴已经坐在家中电脑前，与三五好友相约开黑，一同在战场中激战正酣了吧，身为战场“指挥官”的你，不仅要“眼观六路”观察战局态势，还得“耳听八方”，判断战场中的异动声响，因此拥有一款优秀的游戏耳机，能够给你每一局战斗带来莫大的优势。 …','https://baijiahao.baidu.com/s?id=1705945481156578197&wfr=spider&for=pc');
insert into ws_news values(null,'血手幽灵战略合作STE战队周年庆之际：荣获2021 PEN S1赛季总冠军','2021-06-29','中关村在线','血手幽灵战略合作STE战队全称为：SIX TWO EIGHT（628），"STE"除了"628"这个具有特殊含义日期的释义，更蕴含着深层的精神内涵，其中S代表：Struggle（奋斗）；T代表：Time（时光）；E代表：Excellent（卓越）。这意味着："STE电竞俱乐部将秉承奋斗刻苦的深圳精神，砥砺前行，并乘风深圳速度，与时间赛跑，缩短与梦想的距离，继而站在世界之窗——深圳，向全球出发，成就卓越。"此外，因鼠年诞生，所以STE以十二生肖之首生肖鼠为俱乐部标志，有着积极向 …','https://dcdv.zol.com.cn/771/7714390.html');
insert into ws_news values(null,'赛博电竞风，双模畅快享！血手幽灵M70真无线游戏耳机赏玩','2021-01-08','今日头条','近年来，耳机圈内最为火热的莫过于红到发紫的真无线耳机了。在解决了众多先天短板后，音质、降噪、低延迟成为了过去一年中TWS的关键词。在层出不穷的众厂牌新品中，除了传统的音频厂商，手机和外设品牌也纷纷出手。比如国内知名游戏外设“血手幽灵”所发布的一系列主打“游戏”的真无线蓝牙耳机，今天我们要介绍的就是它旗下的M70，一款与网易大作《量子特攻》合作的联合款，快来看下表现如何吧： …','https://www.toutiao.com/i6915285032462451208/?wid=1636470114972');
insert into ws_news values(null,'赛博电竞风，双模畅快享！血手幽灵M70真无线游戏耳机赏玩','2021-01-08','百家号','近年来，耳机圈内最为火热的莫过于红到发紫的真无线耳机了。在解决了众多先天短板后，音质、降噪、低延迟成为了过去一年中TWS的关键词。在层出不穷的众厂牌新品中，除了传统的音频厂商，手机和外设品牌也纷纷出手。比如国内知名游戏外设“血手幽灵”所发布的一系列主打“游戏”的真无线蓝牙耳机，今天我们要介绍的就是它旗下的M70，一款与网易大作《量子特攻》合作的联合款，快来看下表现如何吧： …','http://baijiahao.baidu.com/builder/preview/s?id=1688308212673909779');
insert into ws_news values(null,'老牌大厂的电竞新贵 血手幽灵 bloody M90 真无线主动降噪耳机体验','2021-01-05','太火鸟','提及双飞燕，很多人的第一印象就是皮实、耐用。在当今智能手机称霸的年代，双飞燕也不在局限于PC端的外设，也开始了手机外设的发展，并用上了“血手幽灵bloody”这个高端子品牌，推出了M70、M90这两款TWS，今天我们要聊的是M90这款主打主动降噪、低延迟游戏的真无线耳机。 …','https://www.taihuoniao.com/topic/view/189954');
insert into ws_news values(null,'老牌大厂的电竞新贵 血手幽灵 bloody M90 耳机体验','2021-01-04','今日头条','提及双飞燕，很多人的第一印象就是皮实、耐用。在当今智能手机称霸的年代，双飞燕也不在局限于PC端的外设，也开始了手机外设的发展，并用上了“血手幽灵bloody”这个高端子品牌，推出了M70、M90这两款TWS，今天我们要聊的是M90这款主打主动降噪、低延迟游戏的真无线耳机。 …','https://www.toutiao.com/i6913779254526345736/');
